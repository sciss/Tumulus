import com.typesafe.sbt.packager.linux.LinuxPackageMapping

lazy val baseName       = "Tumulus"
lazy val baseNameL      = baseName.toLowerCase
lazy val gitHost        = "codeberg.org"
lazy val gitUser        = "sciss"
lazy val gitRepo        = baseName
lazy val projectVersion = "0.5.0-SNAPSHOT"

ThisBuild / version       := projectVersion
ThisBuild / organization  := "de.sciss"
ThisBuild / versionScheme := Some("pvp")

lazy val commonSettings = Seq(
  version      := projectVersion,
  description  := "An art project",
  organization := "de.sciss",
  homepage     := Some(url(s"https://$gitHost/$gitUser/$gitRepo")),
  licenses     := Seq("agpl v3+" -> url("http://www.gnu.org/licenses/agpl-3.0.txt")),
  scalaVersion := "2.13.11",
  scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature", "-encoding", "utf8", "-Xlint")
)

lazy val piMain     = "de.sciss.tumulus.Main"
lazy val soundMain  = "de.sciss.tumulus.sound.Main"
lazy val lightMain  = "de.sciss.tumulus.light.Main"

lazy val buildInfoSettings = Seq(
  // ---- build info ----
  buildInfoKeys := Seq(name, organization, version, scalaVersion, description,
    BuildInfoKey.map(homepage) { case (k, opt)           => k -> opt.get },
    BuildInfoKey.map(licenses) { case (_, Seq((lic, _))) => "license" -> lic }
  ),
  buildInfoOptions += BuildInfoOption.BuildTime
)

lazy val deps = new {
  val main = new {
    val akka                = "2.6.20" // N.B. should match with FScape's
    val audioWidgets        = "2.4.3"
    val equal               = "0.1.6"
    val fileUtil            = "1.1.3"
    val fscape              = "3.15.3"
    val jRPiCam             = "0.2.0"
    val kollFlitz           = "0.2.4"
    val model               = "0.3.5"
    val numbers             = "0.2.0"
    val processor           = "0.5.0"
    val scalaColliderSwing  = "2.9.2"
    val scalaOSC            = "1.3.1"
    val scopt               = "4.1.0"
    val semVerFi            = "0.3.0"
    val soundProcesses      = "4.14.7"
    val sshj                = "0.26.0"
    val submin              = "0.3.5"
    val swingPlus           = "0.5.0"
    val virtualKeyboard     = "1.0.0"
  }
}

lazy val root = project.withId(baseNameL).in(file("."))
//  .dependsOn(pi, sound, light, work)
  .aggregate(pi, sound, light, work)
  .settings(
    name := baseName,
  )

lazy val work = project.withId(s"$baseName-work").in(file("work"))
  .dependsOn(common)
  .settings(commonSettings)
  .settings(
    name := s"$baseName-work",
    libraryDependencies ++= Seq(
      "de.sciss" %% "fscape-lucre"          % deps.main.fscape,
      "de.sciss" %% "soundprocesses-views"  % deps.main.soundProcesses,
    )
  )

lazy val common = project.withId(s"$baseNameL-common").in(file("common"))
  .enablePlugins(BuildInfoPlugin)
  .settings(buildInfoSettings)
  .settings(commonSettings)
  .settings(
    name := s"$baseName-Common",
    buildInfoPackage := "de.sciss.tumulus",
    libraryDependencies ++= Seq(
      "de.sciss"          %% "equal"      % deps.main.equal,
      "de.sciss"          %% "fileutil"   % deps.main.fileUtil,
      "de.sciss"          %% "kollflitz"  % deps.main.kollFlitz,
      "de.sciss"          %% "numbers"    % deps.main.numbers,
      "de.sciss"          %% "processor"  % deps.main.processor,
      "de.sciss"          %% "swingplus"  % deps.main.swingPlus,
      "com.github.scopt"  %% "scopt"      % deps.main.scopt,
      "com.hierynomus"    %  "sshj"       % deps.main.sshj,
    )
  )

lazy val pi = project.withId(piNameL).in(file("pi"))
  .dependsOn(common)
  .enablePlugins(JavaAppPackaging, DebianPlugin)
  .settings(commonSettings)
  .settings(
    name := piName,
//    buildInfoPackage := "de.sciss.tumulus",
    libraryDependencies ++= Seq(
      "de.sciss"    %% "audiowidgets-app"     % deps.main.audioWidgets,
      "de.sciss"    %  "jrpicam"              % deps.main.jRPiCam,
      "de.sciss"    %% "model"                % deps.main.model,
      "de.sciss"    %% "semverfi"             % deps.main.semVerFi,
      "de.sciss"    %% "soundprocesses-views" % deps.main.soundProcesses,
      "de.sciss"    %  "submin"               % deps.main.submin,
      "de.sciss"    %  "virtualkeyboard"      % deps.main.virtualKeyboard,
    ),
    Compile / mainClass := Some(piMain),
  )
  .settings(piDebianSettings)

lazy val sound = project.withId(soundNameL).in(file("sound"))
  .dependsOn(common)
  .enablePlugins(JavaAppPackaging, DebianPlugin)
  .settings(commonSettings)
  .settings(
    name := soundName,
    libraryDependencies ++= Seq(
      "de.sciss"          %% "fscape-lucre"             % deps.main.fscape,
      "de.sciss"          %% "audiowidgets-app"         % deps.main.audioWidgets,
      "de.sciss"          %% "soundprocesses-views"     % deps.main.soundProcesses,
      "de.sciss"          %% "scalacolliderswing-core"  % deps.main.scalaColliderSwing,
      "de.sciss"          %  "submin"                   % deps.main.submin,
      "com.typesafe.akka" %% "akka-actor"               % deps.main.akka,
    ),
    Compile / mainClass := Some(soundMain),
  )
  .settings(soundDebianSettings)

lazy val light = project.withId(lightNameL).in(file("light"))
  .dependsOn(common)
  .enablePlugins(JavaAppPackaging, DebianPlugin)
  .settings(commonSettings)
  .settings(
    name := lightName,
    libraryDependencies ++= Seq(
      "de.sciss" %% "scalaosc" % deps.main.scalaOSC,
    ),
    Compile / mainClass := Some(lightMain),
  )
  .settings(lightDebianSettings)

// ---- debian package ----

lazy val maintainerHH = "Hanns Holger Rutz <contact@sciss.de>"

lazy val piName  = s"$baseName-Pi"
lazy val piNameL = piName.toLowerCase

lazy val soundName  = s"$baseName-Sound"
lazy val soundNameL = soundName.toLowerCase

lazy val lightName  = s"$baseName-Light"
lazy val lightNameL = lightName.toLowerCase

lazy val piDebianSettings = useNativeZip ++ Seq[Def.Setting[_]](
  executableScriptName /* in Universal */ := piNameL,
  scriptClasspath /* in Universal */ := Seq("*"),
  Debian / name         := piNameL,
  Debian / packageName  := piNameL,
  Linux / name          := piNameL,
  Linux / packageName   := piNameL,
  Debian / mainClass    := Some(piMain),
  Debian / maintainer   := maintainerHH,
  Debian / debianPackageDependencies += "java8-runtime",
  Debian / packageSummary := description.value,
  Debian / packageDescription :=
    s"""Software for an art installation - $piName.
       |""".stripMargin
) ++ commonDebianSettings

lazy val soundDebianSettings = useNativeZip ++ Seq[Def.Setting[_]](
  executableScriptName /* in Universal */ := soundNameL,
  scriptClasspath /* in Universal */ := Seq("*"),
  Debian / name         := soundNameL,
  Debian / packageName  := soundNameL,
  Linux / name          := soundNameL,
  Linux / packageName   := soundNameL,
  Debian / mainClass    := Some(soundMain),
  Debian / maintainer   := maintainerHH,
  Debian / debianPackageDependencies += "java8-runtime",
  Debian / packageSummary := description.value,
  Debian / packageDescription :=
    s"""Software for an art installation - $soundName.
       |""".stripMargin
) ++ commonDebianSettings

lazy val lightDebianSettings = useNativeZip ++ Seq[Def.Setting[_]](
  Universal / javaOptions += "-Djava.library.path=/usr/share/tumulus-light/lib/",
  executableScriptName /* in Universal */ := lightNameL,
  scriptClasspath /* in Universal */ := Seq("*"),
  Debian / name         := lightNameL,
  Debian / packageName  := lightNameL,
  Linux / name          := lightNameL,
  Linux / packageName   := lightNameL,
  Debian / mainClass    := Some(lightMain),
  Debian / maintainer   := maintainerHH,
  Debian / debianPackageDependencies += "java8-runtime",
  Debian / packageSummary := description.value,
  Debian / packageDescription :=
    s"""Software for an art installation - $lightName.
      |""".stripMargin
) ++ commonDebianSettings

lazy val commonDebianSettings = Seq(
  // include all files in src/debian in the installed base directory
  Debian / linuxPackageMappings ++= {
    val n     = (Debian / name            ).value.toLowerCase
    val dir   = (Debian / sourceDirectory ).value / "debian"
    val f1    = (dir * "*").filter(_.isFile).get  // direct child files inside `debian` folder
    val f2    = ((dir / "doc") * "*").get
    //
    def readOnly(in: LinuxPackageMapping) =
      in.withUser ("root")
        .withGroup("root")
        .withPerms("0644")  // http://help.unc.edu/help/how-to-use-unix-and-linux-file-permissions/
    //
    val aux   = f1.map { fIn => packageMapping(fIn -> s"/usr/share/$n/${fIn.name}") }
    val doc   = f2.map { fIn => packageMapping(fIn -> s"/usr/share/doc/$n/${fIn.name}") }
    (aux ++ doc).map(readOnly)
  }
)

