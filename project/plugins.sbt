addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.1")    // build debian package
addSbtPlugin("com.eed3si9n"     % "sbt-buildinfo"       % "0.11.0")   // provides version information to copy into main class
addSbtPlugin("com.eed3si9n"     % "sbt-assembly"        % "2.1.1" )   // fat jars